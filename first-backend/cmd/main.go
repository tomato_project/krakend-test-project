package main

import (
	"github.com/gin-gonic/gin"
	"log"
)

func main() {
	server := gin.Default()

	rg := server.Group("/api")

	rg.GET("/health/first", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"result": "successfully from first backend",
		})
	})

	rg.POST("/auth/get-token", func(c *gin.Context) {

		c.JSON(200, gin.H{
			"access_token": map[string]interface{}{
				"aud":   "http://api.example.com",
				"iss":   "https://krakend.io",
				"sub":   "1234567890qwertyuio",
				"jti":   "mnb23vcsrt756yuiomnbvcx98ertyuiop",
				"roles": []interface{}{"user", "admin"},
				"exp":   1735689600,
			},
			"refresh_token": map[string]interface{}{
				"aud": "http://api.example.com",
				"iss": "https://krakend.io",
				"sub": "1234567890qwertyuio",
				"jti": "mnb23vcsrt756yuiomn12876bvcx98ertyuiop",
				"exp": 1735689600,
			},
		})
	})

	if err := server.Run(":4050"); err != nil {
		log.Fatal(err)
	}
}
