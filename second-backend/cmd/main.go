package main

import (
	"github.com/gin-gonic/gin"
	"log"
)

func main() {
	server := gin.Default()

	rg := server.Group("/api")

	rg.GET("/health/second", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"result": "successfully from second backend",
		})
	})

	rg.GET("/protected", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"result": "done",
		})
	})

	if err := server.Run(":4050"); err != nil {
		log.Fatal(err)
	}
}
